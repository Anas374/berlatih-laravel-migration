<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::GET('/', 'IndexController@index');

Route::GET('/register', 'AuthController@formSignUp');

Route::POST('/welcome', 'AuthController@submitForm');

Route::GET('/dashboard', function(){
    return view('dashboard');
});

Route::GET('/table', function(){
    return view('tables.table');
});

Route::GET('/data-tables', function(){
    return view('tables.data-tables');
});